﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Parse;

namespace ConcertStream.Droid
{
	[Activity (Label = "ConcertStream.Droid", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			ParseClient.Initialize("a7TnoQknxzhYtV0XXdxScRQfcVbKUu2Dhmqs5xGD", "tM6i7aeTYUZNMxKgyDW5koN3L0bceTnzCkMwvZEE");

			//DataLayer.MyClass.DoStuff ();

			LoadApplication (new App ());
		}
	}
}

