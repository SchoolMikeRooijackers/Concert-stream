﻿using System;
using System.Collections.Generic;
//using Parse;

using Xamarin.Forms;

namespace ConcertStream
{
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();
		}

		async void OnRegisterClicked(object sender, EventArgs e) {

			IData data = DependencyService.Get<IData> ();

			if (password.Text == passwordRepeat.Text) {

				bool succeed = await data.Register (email.Text, username.Text, password.Text);

				if (succeed) { 
					await Navigation.PushModalAsync (new LoginPage ());
				} else {
					error.Text = "There is already an account created";
				}
			} else {
				error.Text = "Password not repeated";
			}
		}

		async void OnLoginClicked(object sender, EventArgs e) {

			await Navigation.PushModalAsync (new LoginPage ());
		}
	}
}

