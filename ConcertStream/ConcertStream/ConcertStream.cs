﻿using System;
//using Parse;

using Xamarin.Forms;

namespace ConcertStream
{
	public class App : Application
	{
		public App ()
		{
			//MainPage = new ContentPage (new MainPage ());

			//ParseClient.Initialize("a7TnoQknxzhYtV0XXdxScRQfcVbKUu2Dhmqs5xGD", "tM6i7aeTYUZNMxKgyDW5koN3L0bceTnzCkMwvZEE");

			MainPage = new NavigationPage (new ConcertStream.LoginPage ());


		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

