﻿using System;
using System.Threading.Tasks;

namespace ConcertStream
{
	public interface IData
	{
		Task<bool> Register(string email, string username, string password);

		Task<bool> LoginUser(string username, string password);

		bool UserIsLoggedIn ();

		bool UserLogout();
	}
}

