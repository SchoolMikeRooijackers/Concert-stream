﻿

using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;

//using DataLayer;

namespace ConcertStream
{
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			IData data = DependencyService.Get<IData> ();

			if (data.UserIsLoggedIn()) {
				Navigation.PushModalAsync (new MainPage ());
			} else {
				InitializeComponent ();
			}
		}

		async void OnRegisterClicked(object sender, EventArgs e) {
			await Navigation.PushModalAsync (new RegisterPage ());
		}

		async void OnLoginClicked (object sender, EventArgs e)
		{
			IData data = DependencyService.Get<IData> ();

			bool success = await data.LoginUser (username.Text, password.Text);

			if (success) {
				//await Navigation.PushModalAsync (new MainPage ());
				await Navigation.PushModalAsync (new MainPage ());
			} else {
				await DisplayAlert ("Mistake?", "Incorrect login", "OK");
			}
		}
	}
}

