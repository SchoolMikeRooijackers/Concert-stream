﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ConcertStream
{
	public partial class WatchStream : ContentPage
	{
		public WatchStream (string name)
		{
			InitializeComponent ();

			stream.Text = "Watching stream " + name;
		}

		async void OnBackClicked (object sender, EventArgs e)
		{
			await Navigation.PushModalAsync (new MainPage ());
		}
	}
}

