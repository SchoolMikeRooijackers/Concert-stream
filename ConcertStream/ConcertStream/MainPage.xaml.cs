﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ConcertStream
{
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			InitializeComponent ();

			List<String> items = new List<String> ();
			items.Add ("Stream 1");
			items.Add ("Stream 2");


			lv.ItemsSource = items;

			lv.ItemSelected += OnSelection;
			//lv.ItemSelected(
		}

		async void OnSelection (object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem == null) {
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
			}

			await Navigation.PushModalAsync(new WatchStream(e.SelectedItem.ToString()));

			//DisplayAlert ("Item Selected", e.SelectedItem.ToString (), "Ok");
			//((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
		}

		async void OnLogoutClicked (object sender, EventArgs e)
		{
			IData data = DependencyService.Get<IData> ();

		    data.UserLogout ();

			await Navigation.PushModalAsync (new LoginPage ());
		}
	}
}

