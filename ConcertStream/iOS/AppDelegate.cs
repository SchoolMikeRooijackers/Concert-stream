﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Parse;


namespace ConcertStream.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			ParseClient.Initialize("a7TnoQknxzhYtV0XXdxScRQfcVbKUu2Dhmqs5xGD", "tM6i7aeTYUZNMxKgyDW5koN3L0bceTnzCkMwvZEE");

			LoadApplication (new App ());


			/*
			try {


				var user = new ParseUser()
				{
					Username = "my name",
					Password = "my pass",
					Email = "email@example.com"
				};

				user.SignUpAsync();

			}
			catch (Exception err) {
				System.Diagnostics.Debug.WriteLine (err.StackTrace);

			}*/

			//DataLayer.MyClass.DoStuff ();

			return base.FinishedLaunching (app, options);
		}
	}
}

