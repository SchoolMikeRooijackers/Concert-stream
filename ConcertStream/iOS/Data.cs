﻿/*
using System;
using DataLayer;
using Parse;
using ConcertStream;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(ConcertStream.iOS.Data))]
namespace ConcertStream.iOS
{
	public class Data : IData
	{
		public Data mySelf;
		User user;

		public Data()
		{
			mySelf = this;
			user = new User ();
		}
			


		public bool Register (string email, string username, string password)
		{
			return user.Register (email, username, password);
		}

		public async Task<bool> LoginUser (String username, String password)
		{

			await ParseUser.LogInAsync (username, password);

			return true;
		}

		/*
		public async void Login(string username, string password, LoginPage lp) {
			//await user.Login (username, password, lp);
			bool asd = await user.LoginUserAsync(username, password);


			//lp.loginResponse (true);

			/*
			try
			{
				await ParseUser.LogInAsync(username, password);
			}
			catch (Exception e)
			{
				lp.loginResponse (false);
			}

			lp.loginResponse (true);
		}*/
/*
		public bool UserIsLoggedIn () {
			return user.UserIsLoggedIn ();
		}


		public bool UserLogout() {
			return user.UserLogout ();
		}
	}
}
*/
