﻿using System;
using DataLayer;
using Parse;
using ConcertStream;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(DataLayer.Data))]
namespace DataLayer
{
	public class Data : IData
	{
		public Data mySelf;

		public Data()
		{
			mySelf = this;
		}
			
		public async Task<bool> Register (string email, string username, string password)
		{
			try {
				var user = new ParseUser()
				{
					Username = username,
					Password = password,
					Email = email
				};

				await user.SignUpAsync();
			}
			catch (Exception err) {
				System.Diagnostics.Debug.WriteLine (err.StackTrace);
				return false;
			}

			return true;
		}

		public async Task<bool> LoginUser (String username, String password)
		{
			try {
				await ParseUser.LogInAsync (username, password);
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine (e);
				return false;
			}

			return true;
		}

		public bool UserIsLoggedIn () {
			if (ParseUser.CurrentUser != null) {
				return true;
			} else { 
				return false;
			}
		}


		public bool UserLogout() {
			try {
				ParseUser.LogOut ();
			}
			catch (Exception e) {
				System.Diagnostics.Debug.WriteLine (e);
				return false;
			}

			return true;
		}
	}
}

