﻿using System;
using Parse;
using ConcertStream;
using System.Threading.Tasks;

namespace DataLayer
{
	public class User
	{
		public User ()
		{
		}

		//public async Task<>

		/*
		public async Task<bool> LoginUserAsync (String username, String password)
		{
				

		}*/

		/*
		public async void Login() {

			//LoginPage lp = new LoginPage ();
			//lp.loginResponse (true);
		}*/

		/*
		public async void Login(string username, string password, LoginPage lp) {

			//LoginPage lp = new LoginPage ();
			lp.loginResponse (true);

			//ConcertStream.LoginPag

			/*
			try
			{
				ParseUser.LogInAsync(username, password);
			}
			catch (Exception e)
			{
				return false;
			}

			return true;
		}*/

		public bool Register(string email, string username, string password) {

			try {
				var user = new ParseUser()
				{
					Username = username,
					Password = password,
					Email = email
				};

				user.SignUpAsync();
			}
			catch (Exception err) {
				System.Diagnostics.Debug.WriteLine (err.StackTrace);
				return false;
			}

			return true;
		}

		public bool UserIsLoggedIn() {
			if (ParseUser.CurrentUser != null) {
				return true;
			} else { 
				return false;
			}
		}

		public bool UserLogout() {
			try {
				ParseUser.LogOut ();
			}
			catch (Exception e) {
				return false;
			}

			return true;
		}
	}
}

