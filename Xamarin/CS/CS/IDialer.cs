﻿using System;

namespace CS
{
	public interface IDialer
	{
		bool Dial(string number);
	}
}

