Bezoekers van een popconcert kunnen (indien toegestaan door de organisatie van het concert) met deze app live video (en muziek) streamen naar een server. Gebruikers kunnen dan op die server inloggen en het concert live volgen.

Als er meerdere gebruikers van deze app op het zelfde popconcert aanwezig zijn dan kunnen gebruikers die de website bekijken schakelen tussen verschillende "camera-views".

Ook gebruikers die de app gebruiken kunnen de view van andere filmers bekijken.